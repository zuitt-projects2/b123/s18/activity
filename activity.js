let trainer = {
	name: "Luffy",
	age: 15,
	address: {
		city: "East Blue",
		country:"One piece",
	},
	friends:["Zoro","Sanji"],
	pokemons:["Charmander","Squirtle","Balbasuar"], 
	catch: function(nameOfPokemon){
		if(trainer.pokemons.length === 6){
			console.log("A trainer should only have 6 pokemons to carry.")
		}else {
			trainer.pokemons.push(nameOfPokemon)
			console.log(`Gotcha, ${nameOfPokemon}`)
		}
	},
	release:function() {
		if(trainer.pokemons.length === 0){
			console.log("You have no more pokemons! Catch one first.")
		} else {
			trainer.pokemons.pop()
		}
	}
}
	function Pokemon(name,type,level) {
	this.name = name
	this.type = type
	this.level = level
	this.isFainted = false
	this.hp = level*3
	this.atk = level*2.5
	this.def = level*2
	this.tackle = function(pokemon) {
		console.log(`${pokemon.name} tackled ${pokemon.name}`)
	}

	}

	let pokemon1 = new Pokemon("Charmander","fire",10)
	console.log(pokemon1)
	let pokemon2 = new Pokemon("Pikachu","lightning",11)
	console.log(pokemon2)
	

console.log(trainer)